package org.example.decerto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DecertoTestApp {

    public static void main(final String[] args) {
        SpringApplication.run(DecertoTestApp.class, args);
    }

}
