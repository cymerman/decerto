package org.example.decerto.aggregation;

public abstract class ObjectAggregator<T> {

    public abstract T aggregate(T value1, T value2);
}
