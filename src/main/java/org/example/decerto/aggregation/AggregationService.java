package org.example.decerto.aggregation;

import lombok.RequiredArgsConstructor;
import org.example.decerto.random.java.RandomJavaNumberGenerator;
import org.example.decerto.random.org.RandomOrgNumberGenerator;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AggregationService {

    private final RandomOrgNumberGenerator  randomOrgNumberGenerator;
    private final RandomJavaNumberGenerator randomJavaNumberGenerator;
    private final NumberAggregator numberAggregator;

    public Integer aggregateNumbers() {
        return numberAggregator.aggregate(randomOrgNumberGenerator.getNumber(), randomJavaNumberGenerator.getNumber());
    }
}
