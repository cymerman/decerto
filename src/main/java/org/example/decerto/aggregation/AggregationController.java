package org.example.decerto.aggregation;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/aggregate")
@AllArgsConstructor
public class AggregationController {

    private final AggregationService aggregationService;

    @GetMapping("/numbers")
    public Integer aggregateNumbers() {
        return aggregationService.aggregateNumbers();
    }

}
