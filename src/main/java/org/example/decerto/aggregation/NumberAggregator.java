package org.example.decerto.aggregation;

import org.springframework.stereotype.Component;

@Component
public class NumberAggregator extends ObjectAggregator<Integer>{

    public Integer aggregate(final Integer value1, final Integer value2) {
        return Math.addExact(value1, value2);
    }
}
