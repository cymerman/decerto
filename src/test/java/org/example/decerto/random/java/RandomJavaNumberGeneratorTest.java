package org.example.decerto.random.java;

import org.example.decerto.BaseTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import static org.assertj.core.api.Assertions.assertThat;

class RandomJavaNumberGeneratorTest extends BaseTest {

    @Value("${generator.number.java.param.min}")
    private Integer min;

    @Value("${generator.number.java.param.max}")
    private Integer max;

    @Autowired
    private RandomJavaNumberGenerator randomJavaNumberGenerator;

    @Test
    void shouldGenerateUtilRandom() {
        final Integer result = randomJavaNumberGenerator.getNumber();

        assertThat(result).isGreaterThan(min);
        assertThat(result).isLessThan(max);
    }
}
