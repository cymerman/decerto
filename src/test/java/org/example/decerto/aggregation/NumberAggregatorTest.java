package org.example.decerto.aggregation;

import org.example.decerto.BaseTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

class NumberAggregatorTest extends BaseTest {

    @Autowired
    private NumberAggregator numberAggregator;

    @Test
    void shouldAggregateIntegers() {
        final Integer aggregatedInteger = numberAggregator.aggregate(1, 2);
        assertThat(aggregatedInteger).isEqualTo(3);
    }
}
