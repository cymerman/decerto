package org.example.decerto.aggregation;

import com.github.tomakehurst.wiremock.client.WireMock;
import org.assertj.core.api.Assertions;
import org.example.decerto.BaseTest;
import org.junit.jupiter.api.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.client.WireMock.configureFor;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class AggregationControllerTest extends BaseTest {

    @Test
    void shouldIntegrateTwoNumbers() throws Exception {
        configureFor("localhost", wireMockServer.port());
        wireMockServer.stubFor(
                WireMock.get(
                        urlEqualTo("/integers/?num=1&min=0&max=100&col=1&base=10&format=plain"))
                        .willReturn(
                                aResponse()
                                        .withStatus(200)
                                        .withBody("11\n")));


        final String aggregationResult = mockMvc.perform(get("/aggregate/numbers"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        final Integer result = Integer.valueOf(aggregationResult);

        Assertions.assertThat(result).isPositive();
    }
}
