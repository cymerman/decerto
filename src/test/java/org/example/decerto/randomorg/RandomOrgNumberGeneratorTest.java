package org.example.decerto.randomorg;

import com.github.tomakehurst.wiremock.client.WireMock;
import org.example.decerto.BaseTest;
import org.example.decerto.random.org.RandomOrgNumberGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.configureFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.assertj.core.api.Assertions.assertThat;

public class RandomOrgNumberGeneratorTest extends BaseTest {

    @Autowired
    private RandomOrgNumberGenerator randomOrgNumberGenerator;

    @Test
    public void shouldGetPositiveNumber() {
        configureFor("localhost", wireMockServer.port());
        wireMockServer.stubFor(
                WireMock.get(
                        urlEqualTo("/integers/?num=1&min=0&max=100&col=1&base=10&format=plain"))
                        .willReturn(
                                aResponse()
                                        .withStatus(200)
                                        .withBody("12\n")));

        assertThat(randomOrgNumberGenerator.getNumber()).isEqualTo(12);
    }
}
